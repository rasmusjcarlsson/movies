import { render, screen } from "@testing-library/react";
import Details from "./";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => ({
    pathname: "/details/414906",
  }),
}));

const results = {
  id: 414906,
  original_title: "The Batman",
};

beforeAll(() => jest.spyOn(window, "fetch"));

beforeEach(() => {
  fetch.mockClear();
});

describe("<ViatelMovies />", () => {
  it("renders movie title", async () => {
    window.fetch.mockResolvedValueOnce({
      json: async () => results,
    });
    render(<Details />);

    expect(await screen.findByText("The Batman")).toBeTruthy();
  });
});
