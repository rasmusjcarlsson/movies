import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Section from "../../components/Section";
import PageTitle from "../../components/PageTitle";
import { Button } from "@mui/material";
import {
  StyledRow,
  Spacer,
  MoviePoster,
  StyledText,
  CompanyLogo,
  InfoContainer,
} from "./style";

const Details = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  const { id } = useParams();
  const navigate = useNavigate();
  const imgDB = "https://image.tmdb.org/t/p/w500/";

  useEffect(() => {
    document.title = "details";
  }, []);

  useEffect(() => {
    setLoading(true);
    fetch(
      `https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.REACT_APP_API_KEY}`
    )
      .then((res) => res.json())
      .then((json) => {
        setData(json);
        setLoading(false);
      });
  }, [id]);

  const handleBack = () => {
    navigate("/");
  };

  return (
    <Section>
      <PageTitle />
      <Button onClick={handleBack}>Back</Button>
      {!data || loading ? (
        <p>...loading</p>
      ) : (
        <>
          <StyledRow>
            <MoviePoster
              src={imgDB + data?.poster_path}
              alt={data?.original_title}
            />
            <Spacer horizontal="20px" />
            <InfoContainer>
              <StyledText variant="h3">{data.original_title}</StyledText>
              {data?.tagline && (
                <StyledText variant="h5">"{data?.tagline}"</StyledText>
              )}
              <StyledText variant="body1">{data?.overview}</StyledText>
              <StyledRow>
                {data?.genres?.map((genre, index) => (
                  <StyledText variant="body2" key={genre.id}>
                    {genre.name + (index + 1 !== data.genres.length ? "," : "")}
                  </StyledText>
                ))}
              </StyledRow>
              <Spacer horizontal="100px" />
              {data?.production_companies?.map((company) => {
                const companyLogo = company.logo_path ? (
                  <CompanyLogo
                    key={company.id}
                    alt={company.name}
                    src={imgDB + company.logo_path}
                  />
                ) : (
                  <StyledText key={company.id} variant="body2">
                    {company.name}
                  </StyledText>
                );

                return companyLogo;
              })}
              <Spacer horizontal="20px" />
              <a href={data?.homepage}>{data?.homepage}</a>

              <Spacer horizontal="20px" />
              <StyledText variant="body2">STATUS: {data?.status}</StyledText>
              <StyledText variant="body2">
                RELEASE DATE: {data?.release_date}
              </StyledText>
              <StyledText variant="body2">
                BUDGET: {data?.budget}$USD
              </StyledText>
            </InfoContainer>
          </StyledRow>
        </>
      )}
    </Section>
  );
};

export default Details;
