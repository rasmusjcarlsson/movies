import styled from "@emotion/styled";
import { Typography } from "@mui/material";

export const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
`;

export const Spacer = styled.div`
  height: ${(props) => (props.horizontal ? props.horizontal : "0px")};
  width: ${(props) => (props.vertical ? props.vertical : "0px")};
`;

export const MoviePoster = styled.img`
  max-height: 600px;
  margin-right: 20px;
`;
export const CompanyLogo = styled.img`
  height: 50px;
  margin-right: 20px;
`;

export const StyledText = styled(Typography)`
  margin-right: 5px;
  margin-bottom: 2px;
`;

export const InfoContainer = styled.div`
  width: 40%;
`;
