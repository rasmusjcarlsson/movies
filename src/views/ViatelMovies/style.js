import { Card, TextField } from "@mui/material";
import styled from "@emotion/styled";

export const InfoBox = styled.div`
  position: relative;
  max-width: 375px;
  max-height: 375px;
  background-color: white;
`;

export const StyledImage = styled.img`
  height: 300px;
  margin-right: 5px;
`;

export const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
`;

export const FlexEndContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const CaptionText = styled.p`
  display: -webkit-box;
  max-height: 150px;
  max-width: 200px;
  -webkit-line-clamp: 8;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;

export const CaptionTitle = styled.span`
  font-weight: bold;
`;

export const StyledCard = styled(Card)`
  background-color: #fffafa;
  padding: 8px;
`;

export const StyledTextField = styled(TextField)`
  margin-bottom: 59px;
  width: 33%;
`;
