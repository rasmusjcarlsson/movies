import { render, screen } from "@testing-library/react";
import ViatelMovies from "./";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => ({
    pathname: "/",
  }),
}));

const results = [
  {
    id: 414906,
    original_language: "en",
    original_title: "The Batman",
    overview:
      "In his second year of fighting crime, Batman uncovers corruption in Gotham City that connects to his own family while facing a serial killer known as the Riddler.",
    poster_path: "/74xTEgt7R36Fpooo50r9T25onhq.jpg",
    release_date: "2022-03-01",
    title: "The Batman",
  },
];

beforeAll(() => jest.spyOn(window, "fetch"));

beforeEach(() => {
  fetch.mockClear();
});

describe("<ViatelMovies />", () => {
  it("renders movie title", async () => {
    window.fetch.mockResolvedValueOnce({
      json: async () => ({ results }),
    });
    render(<ViatelMovies />);
    expect(await screen.findByText("The Batman")).toBeTruthy();
  });
});
