import React, { useEffect, useState } from "react";
import Section from "../../components/Section";
import PageTitle from "../../components/PageTitle";
import { Grid, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import {
  InfoBox,
  StyledImage,
  StyledRow,
  CaptionText,
  CaptionTitle,
  FlexEndContainer,
  StyledCard,
  StyledTextField,
} from "./style";

const ViatelMovies = () => {
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");
  const [data, setData] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    document.title = "Viatel";
  }, []);

  useEffect(() => {
    setLoading(true);
    if (search)
      fetch(
        `https://api.themoviedb.org/3/search/movie?api_key=${process.env.REACT_APP_API_KEY}&query=${search}`
      )
        .then((res) => res.json())
        .then((json) => {
          setData(json);
          setLoading(false);
        });
    else
      fetch(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${process.env.REACT_APP_API_KEY}`
      )
        .then((res) => res.json())
        .then((json) => {
          setData(json);
          setLoading(false);
        });
  }, [search]);

  const handleSearch = (value) => {
    setSearch(value);
  };

  const handleReadMore = (id) => {
    navigate("/details/" + id);
  };

  return (
    <>
      <Section>
        <PageTitle />
        <StyledTextField
          id="standard-basic"
          label="Search"
          variant="standard"
          onChange={(e) => handleSearch(e.target.value)}
        />
        {loading ? (
          <p>...loading</p>
        ) : (
          <Grid container spacing={3}>
            {data?.results.map((movie) => {
              return (
                <Grid item key={movie.id} xs={12} sm={6} xl={4}>
                  <StyledCard>
                    <h2>{movie.original_title}</h2>
                    <StyledRow>
                      <StyledImage
                        src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
                        alt={movie.original_title}
                      />
                      <InfoBox>
                        <CaptionText>
                          <CaptionTitle>Overview:</CaptionTitle>{" "}
                          {movie.overview}
                        </CaptionText>
                        <CaptionText>
                          <CaptionTitle>Language:</CaptionTitle>{" "}
                          {movie.original_language.toUpperCase()}
                        </CaptionText>
                        {movie.release_date && (
                          <CaptionText>
                            <CaptionTitle>Release date:</CaptionTitle>{" "}
                            {movie.release_date.toUpperCase()}
                          </CaptionText>
                        )}
                      </InfoBox>
                    </StyledRow>
                    <FlexEndContainer>
                      <Button onClick={() => handleReadMore(movie.id)}>
                        Read more
                      </Button>
                    </FlexEndContainer>
                  </StyledCard>
                </Grid>
              );
            })}
          </Grid>
        )}
      </Section>
    </>
  );
};

export default ViatelMovies;
