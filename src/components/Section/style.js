import styled from "styled-components";

export const StyledSection = styled.div`
  width: auto;
  max-width: 1400px;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  z-index: 100;
  position: relative;
`;
