import { render, screen } from "@testing-library/react";
import Section from "./";

describe("<Section />", () => {
  const p = <p>child</p>;
  it("renders title", () => {
    render(<Section>{p}</Section>);
    expect(screen.getByText("child")).toBeTruthy();
  });
});
