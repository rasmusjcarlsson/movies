import React from "react";
import { HomeTitle, Subtitle } from "./style";

const PageTitle = () => {
  return (
    <HomeTitle>
      Viatel - <Subtitle>Movies</Subtitle>
    </HomeTitle>
  );
};

export default PageTitle;
