import styled from "styled-components";

export const HomeTitle = styled.h1`
  text-transform: capitalize;
`;

export const Subtitle = styled.span`
  font-size: 28px;
  font-weight: 500;
  text-transform: uppercase;
  letter-spacing: 4px;
  line-height: 3em;
  padding-left: 0.25em;
  color: rgba(0, 0, 0, 0.4);
  padding-bottom: 10px;
`;
