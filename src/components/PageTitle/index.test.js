import { render, screen } from "@testing-library/react";
import PageTitle from "./";

describe("<PageTitle />", () => {
  it("renders title", () => {
    render(<PageTitle />);
    expect(screen.getByText("Viatel -")).toBeTruthy();
    expect(screen.getByText("Movies")).toBeTruthy();
  });
});
