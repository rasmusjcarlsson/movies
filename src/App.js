import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Details from "./views/Details";
import ViatelMovies from "./views/ViatelMovies";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path="/" element={<ViatelMovies />} />
          <Route exact path="details/:id" element={<Details />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
